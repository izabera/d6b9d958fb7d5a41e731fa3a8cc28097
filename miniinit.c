#define _GNU_SOURCE
#include <sys/wait.h>
#include <unistd.h>
#include <signal.h>

int main(int argc, char *argv[]) {
  if (argc < 2) argv = (char *[]) { argv[0], "/bin/sh", NULL };

  int status;
  if (getpid() != 1) return 1;

  pid_t pid = fork();
  switch (pid) {
    case -1: return 1;
    case  0: return execvp(argv[1], argv+1);
  }
  while (1) {
    if (wait(&status) == pid)
      return status;
  }
}
